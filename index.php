<?php

$page_name = 'Главная';

require_once(__DIR__.'/core/header.php');

?>

<section>

	<div class="labels">
		<h3>Информация о магазине</h3>
		<p>В нашем магазине представлены новинки ROCK-музыки.</p>
	</div>
	<hr>

	<?php echo render_albums_list(get_new_albums(), $cart_albums_id); ?>

</section>

<section>

	<hr>

	<div class="labels">
		<h3>Популярные альбомы</h3>
		<p>Ниже представлены популярные альбомы известных рок-исполнителей.</p>
	</div>
	<hr>

	<?php echo render_albums_list(get_top_albums(), $cart_albums_id); ?>

</section>

<?php require_once(__DIR__.'/core/footer.php'); ?>