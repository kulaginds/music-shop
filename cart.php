<?php

$page_name = 'Корзина';

require_once(__DIR__.'/core/header.php');

$cart_albums = get_albums_by_id($cart_albums_id);

if (isset($is_buy_handled)) {
	include(BLOCKS_DIR.'/cart_message.php');
} else {
	include(BLOCKS_DIR.'/cart_list.php');
}

require_once(__DIR__.'/core/footer.php');

?>