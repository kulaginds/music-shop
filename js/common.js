$(document).ready(function() {

	var debug = true;

	function updateCart() { if (debug) { console.log('updateCart'); }
		var total_cost = 0;

		$('section .list:empty').each(function(index, item) {
			$(item).append($('<p />').html('Корзина пуста.'));
		});

		$('*[data-album-id]').each(function(index, item) {
			var cost = $(item).data('album-cost');
			cost     = parseFloat(cost);

			if (isNaN(cost)) {
				return;
			}

			total_cost += cost;
		});

		if (total_cost == 0) {
			$('#total_cost_label').hide();
			$('#order').hide();
		} else {
			$('#total_cost_label').show();
			$('#order').show();
		}

		$('#total_cost').html(total_cost);
	}

	function parseCart() { if (debug) { console.log('parseCart'); }
		var cart = $.cookie('cart');

		if (null == cart || cart.length == 0) {
			cart = [];
		} else {
			cart = cart.split(',').map(function(item, index, arr) {
				return parseInt(item);
			});
		}

		return cart;
	}

	function saveCart(cart) { if (debug) { console.log('saveCart', cart); }
		cart = cart.join(',');

		$.cookie('cart', cart, { expires: 7, path: '/' });
	}

	window.addToCart = function(album_id) { if (debug) { console.log('addToCart', album_id); }
		album_id = parseInt(album_id);

		// check album_id
		if (isNaN(album_id) || album_id <= 0) {
			return;
		}

		var cart = parseCart();

		// is cart has album_id
		if (-1 === cart.indexOf(album_id)) {
			cart.push(album_id);
			$('#cart_counter').html(cart.length);
			saveCart(cart);
		}

		// add to album cart class added
		$('*[data-album-id="' + album_id + '"]').find('.cart').addClass('added');
	}

	window.removeFromCart = function(album_id) { if (debug) { console.log('removeFromCart', album_id); }
		album_id = parseInt(album_id);

		// check album_id
		if (isNaN(album_id) || album_id <= 0) {
			return;
		}

		var cart        = parseCart();
		var album_index = -1;

		// is cart has album_id
		if (-1 !== (album_index = cart.indexOf(album_id))) {
			if (cart.length > 1) {
				// move value with index 0 to album_index
				cart[album_index] = cart[0];
				cart              = cart.splice(1);
			} else {
				cart = [];
			}

			$('#cart_counter').html(cart.length);
			saveCart(cart);
		}

		// add to album cart class added
		$('*[data-album-id="' + album_id + '"]').remove();
		
		// update total cost
		updateCart();
	}

	window.callAction = function(name, params) {
		if (typeof params === 'undefined' || params == null) {
			params = {};
		}

		params.action = name;

		$.redirect(window.location.pathname, params);
	}

	if (typeof HAS_ALBUMS !== 'undefined' && HAS_ALBUMS) { console.log('check HAS_ALBUMS');
		$('#total_cost_label').show();
		$('#order').show();
	}

});