<?php

$page_name = 'Форма диспетчера';
$top_menu  = '/dispatcher_top_menu.php';

require_once(__DIR__.'/core/header.php');

if (check_dispatcher_login()) {
	include(BLOCKS_DIR.'/dispatcher_list.php');
} else {
	include(BLOCKS_DIR.'/dispatcher_login.php');
}

require_once(__DIR__.'/core/footer.php');

?>