<?php if (!defined('SITECORE')) die('Hacking attempt!');

	require_once(ALBUMS_DIR.'/config.php');

	// return array of albums by array id
	function get_albums_by_id($albums_id) {
		global $albums;

		if (is_string($albums_id)) {
			$albums_id = explode_to_int(',', $albums_id);
		}

		return array_intersect_key($albums, array_flip($albums_id));
	}

	// return array of new albums
	function get_new_albums() {
		global $new_albums;

		return get_albums_by_id($new_albums);
	}

	// return array of top albums
	function get_top_albums() {
		global $top_albums;

		return get_albums_by_id($top_albums);
	}

?>