<?php if (!defined('SITECORE')) die('Hacking attempt!');

	// render albums list
	function render_albums_list($albums, $albums_id_in_cart, $is_cart = false) {
		$render = '<div class="list">';

		if (count($albums) > 0) {
			foreach ($albums as $key => $album) {
				extract($album);

				$render .= "<div class=\"item\" data-album-id=\"$key\" data-album-cost=\"$cost\">".
						   "	<img src=\"$img\" alt=\"Обложка альбома группы $name\">";
						   
				if (!$is_cart && in_array($key, $albums_id_in_cart)) {
					$render .= "	<div class=\"cart added\">";
				} else {
					$render .= "	<div class=\"cart\">";
				}

				if ($is_cart) {
					$render .= "		<button type=\"button\" class=\"btn red\" onclick=\"removeFromCart($key);\">Убрать из корзины</button>";
				} else {
					$render .= "		<button type=\"button\" class=\"btn yellow\" onclick=\"addToCart($key);\">Добавить в корзину</button>";
				}

				$render .= "		<p>Альбом в корзине для покупки</p>".
						   "	</div>".
						   "	<h1>$name</h1>".
						   "	<h1 class=\"cost\">$$cost</h1>".
						   "</div>";
			}
		} else {
			$render .= '<p>Корзина пуста.</p>';
		}

		$render .= '</div>';
		$render .= '<div class="clear"></div>';

		return $render;
	}

	// render albums name list
	function render_albums_name_list($albums) {
		$render = '<i>Нет альбомов</i>';

		if (count($albums) > 0) {
			$render   = '';
			$is_first = true;

			foreach ($albums as $album) {
				if ($is_first) {
					$is_first = false;
					$render .= $album['name'];
				} else {
					$render .= '<hr>'.$album['name'];
				}
			}
		}

		return $render;
	}

?>