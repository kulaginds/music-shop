<?php if (!defined('SITECORE')) die('Hacking attempt!');

	$albums = array(
		0 => array(
			'img'  => '/images/300x300.png',
			'name' => 'Test album',
			'cost' => 0.0,
		),
		1 => array(
			'img'  => '/images/metallica.png',
			'name' => 'Metallica - Hardwired…To Self-Destruct (2016)',
			'cost' => 14.99,
		),
		2 => array(
			'img'  => '/images/nickelback.png',
			'name' => 'Nickelback - Dirty Laundry (Single) (2016)',
			'cost' => 0.99,
		),
		3 => array(
			'img'  => '/images/pretty_reckless.png',
			'name' => 'The Pretty Reckless - Take Me Down (Single) (2016)',
			'cost' => 0.99,
		),
		4 => array(
			'img'  => '/images/green_day.png',
			'name' => 'Green Day - Bang Bang (Single) (2016)',
			'cost' => 0.99,
		),
		5 => array(
			'img'  => '/images/skillet.png',
			'name' => 'Skillet - Unleashed (2016)',
			'cost' => 14.99,
		),
		6 => array(
			'img'  => '/images/smyslovyye_gallyutsinatsii.png',
			'name' => 'Смысловые Галлюцинации - Минус один (2016)',
			'cost' => 14.99,
		),
		7 => array(
			'img'  => '/images/ac_dc.png',
			'name' => 'AC/DC - Highway to Hell (1979)',
			'cost' => 14.99,
		),
		8 => array(
			'img'  => '/images/linkin_park.png',
			'name' => 'Linkin Park - Meteora (2003)',
			'cost' => 14.99,
		),
		9 => array(
			'img'  => '/images/van_halen.png',
			'name' => 'Van Halen - Panama (Single) (1984)',
			'cost' => 0.99,
		),
	);

	$new_albums = array(1, 2, 3, 4, 5, 6);
	$top_albums = array(7, 8, 9);

?>