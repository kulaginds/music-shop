<?php if (!defined('SITECORE')) die('Hacking attempt!');
	
	define('CART_NAME_MINLENGTH', 4);
	define('CART_NAME_MAXLENGTH', 40);
	define('CART_PHONE_LENGTH', 11);
	define('CART_ADDRESS_MINLENGTH', 4);
	define('CART_ADDRESS_MAXLENGTH', 400);

	define('CSV_SEPARATOR', ';');
	define('CSV_LINE_MAXLENGTH', CART_NAME_MAXLENGTH + CART_PHONE_LENGTH + CART_ADDRESS_MAXLENGTH + 32 + 1);
	define('CART_BUY_ACTION', 'cart_buy');
	define('CART_INPUT_CHARSET', 'UTF-8');
	define('CART_OUTPUT_CHARSET', 'WINDOWS-1251');

?>