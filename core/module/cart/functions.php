<?php if (!defined('SITECORE')) die('Hacking attempt!');
	
	require_once(CART_DIR.'/constants.php');
	require_once(ALBUMS_DIR.'/functions.php');

	// return array of id of cart albums
	function get_cart_albums_id() {
		$albums = array();

		if (false === array_key_exists('cart', $_COOKIE) || empty($_COOKIE['cart'])) {
			return $albums;
		}

		$albums = explode_to_int(',', $_COOKIE['cart']);

		return $albums;
	}

	// return array of cart albums
	function get_cart_albums() {
		$albums = get_cart_albums_id();

		return get_albums_by_id($albums);
	}

	// return total cost of array of albums
	function get_albums_total_cost($albums) {
		$total_count = 0;

		if (count($albums) == 0) {
			return $total_count;
		}

		foreach ($albums as $album) {
			$total_count += $album['cost'];
		}

		return $total_count;
	}

	// add new order
	function add_order($name, $phone, $address, $albums_id) {
		$fp = @fopen(SECURE_FILE_NEW_ORDERS, 'a+');

		if (false === $fp) {
			return false;
		}

		$albums_id = implode(',', $albums_id);
		$name      = iconv(CART_INPUT_CHARSET, CART_OUTPUT_CHARSET, $name);
		$phone     = iconv(CART_INPUT_CHARSET, CART_OUTPUT_CHARSET, $phone);
		$address   = iconv(CART_INPUT_CHARSET, CART_OUTPUT_CHARSET, $address);
		$hash      = md5($name.$phone.$address.$albums_id);
		$order     = array($hash, $name, $phone, $address, $albums_id);

		if (false === fputcsv($fp, $order, CSV_SEPARATOR)) {
			fclose($fp);
			return false;
		}

		fclose($fp);
		return true;
	}

	// return new orders
	function get_new_orders() {
		$orders = array();
		$fp     = @fopen(SECURE_FILE_NEW_ORDERS, 'r');

		if (false === $fp) {
			return $orders;
		}

		while (false !== ($data = fgetcsv($fp, CSV_LINE_MAXLENGTH, CSV_SEPARATOR))) {
			$orders[] = array(
				$data[0],
				iconv(CART_OUTPUT_CHARSET, CART_INPUT_CHARSET, $data[1]),
				iconv(CART_OUTPUT_CHARSET, CART_INPUT_CHARSET, $data[2]),
				iconv(CART_OUTPUT_CHARSET, CART_INPUT_CHARSET, $data[3]),
				$data[4],
			);
		}

		fclose($fp);

		return $orders;
	}

	// delete all lines with hash
	function delete_orders_by_hash($hash) {
		$fp = @fopen(SECURE_FILE_NEW_ORDERS, 'r');

		if (false === $fp) {
			return;
		}

		$orders = array();

	    while (!feof($fp)) {
	        $order = fgetcsv($fp, CSV_LINE_MAXLENGTH, CSV_SEPARATOR);

	        if ((false !== $order) && ($hash != $order[0])) {
	            $orders[] = $order;
	        }
	    }

	    fclose($fp);

	    $fp = @fopen(SECURE_FILE_NEW_ORDERS, 'w+');

		if (false === $fp) {
			return;
		}

	    foreach ($orders as $order) {
	    	fputcsv($fp, $order, CSV_SEPARATOR);
	    }

	    fclose($fp);
	}

	// return order by hash
	function get_order_by_hash($hash) {
		$fp    = @fopen(SECURE_FILE_NEW_ORDERS, 'r');
		$order = null;

		if (false === $fp) {
			return;
		}

	    while (!feof($fp)) {
	        $line = fgetcsv($fp, CSV_LINE_MAXLENGTH, CSV_SEPARATOR);    

	        if ((false !== $line) && ($hash == $line[0])) {
	            $order = $line;
	            break;
	        }
	    }

	    fclose($fp);

	    return $order;
	}

	// approve order
	function approve_order($hash) {
		$order = get_order_by_hash($hash);

		if (is_null($order)) {
			return false;
		}

		delete_orders_by_hash($hash);

		$fp = @fopen(SECURE_FILE_APPROVED_ORDERS, 'a+');

		if (false === $fp) {
			return false;
		}

		if (false === fputcsv($fp, $order, CSV_SEPARATOR)) {
			fclose($fp);
			return false;
		}

		fclose($fp);
		return true;
	}

	// clear cart
	function clear_cart() {
		setcookie('cart', '', strtotime('+7 days'), '/');
	}

	// handler buy action
	function cart_handle_buy_action() {
		global $message_title, $message_text, $is_buy_handled, $cart_albums_id, $cart_counter;

		$name           = clear_field($_POST['name']);
		$phone          = clear_field($_POST['phone']);
		$address        = clear_field($_POST['address']);
		$is_buy_handled = true;

		$message_title = 'Ошибка в контактных данных';

		if (check_field_length($name, CART_NAME_MINLENGTH, CART_NAME_MAXLENGTH)) {
			$message_text  = 'Поле "Ваше имя" должно быть не меньше '.CART_NAME_MINLENGTH.' и не больше '.CART_NAME_MAXLENGTH.' символов.<br><a href="javascript:window.history.go(-1);">Назад</a>';

			return;
		}

		if (check_field_length($phone, CART_PHONE_LENGTH)) {
			$message_text  = 'Поле "Ваш телефон" должно состоять из '.CART_PHONE_LENGTH.' цифр.<br><a href="javascript:window.history.go(-1);">Назад</a>';

			return;
		}

		if (false === ctype_digit($phone)) {
			$message_text  = 'Поле "Ваш телефон" должно состоять только из цифр.<br><a href="javascript:window.history.go(-1);">Назад</a>';

			return;
		}

		if (check_field_length($address, CART_ADDRESS_MINLENGTH, CART_ADDRESS_MAXLENGTH)) {
			$message_text  = 'Поле "Ваш почтовый адрес" должно быть не меньше '.CART_ADDRESS_MINLENGTH.' и не больше '.CART_ADDRESS_MAXLENGTH.' символов.<br><a href="javascript:window.history.go(-1);">Назад</a>';

			return;
		}

		if (false === add_order($name, $phone, $address, $cart_albums_id)) {
			$message_title = 'Ошибка создания заказа';
			$message_text  = 'Приносим извинения за доставленные неудобства. Повторите покупку позже.';

			return;
		}

		clear_cart();

		$cart_albums_id = array();
		$cart_counter   = 0;
		$message_title  = 'Спасибо за заказ!';
		$message_text   = 'Заказ отправлен на обработку. В ближайшее время с вами свяжется диспетчер для подтверждения заказа.';

		header('Refresh: 5; URL='.HOME_URL);
	}

	// initialization
	$cart_albums_id = get_cart_albums_id();
	$cart_counter   = count($cart_albums_id);

	///////////////////////////
	// Handling cart actions //
	///////////////////////////

	// handling buy action
	if (check_action(CART_BUY_ACTION) && $cart_counter > 0) {
		cart_handle_buy_action();
	}

?>