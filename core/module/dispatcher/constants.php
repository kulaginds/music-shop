<?php if (!defined('SITECORE')) die('Hacking attempt!');
	
	define('DISPATCHER_LOGIN_ACTION', 'dispatcher_login_action');
	define('DISPATCHER_LOGOUT_ACTION', 'dispatcher_logout_action');
	define('DISPATCHER_GET_NEW_ORDERS_ACTION', 'dispatcher_get_new_orders_action');
	define('DISPATCHER_GET_APPROVED_ORDERS_ACTION', 'dispatcher_get_approved_orders_action');
	define('DISPATCHER_APPROVE_ORDER_ACTION', 'dispatcher_approve_order_action');
	define('DISPATCHER_DELETE_ORDER_ACTION', 'dispatcher_delete_order_action');

	define('DISPATCHER_PASSWORD', '123456');
	define('DISPATCHER_AUTH_COOKIE_NAME', 'dispatcher');
	define('DISPATCHER_AUTH_COOKIE_MAXTIME', strtotime('+1 day'));

?>