<?php if (!defined('SITECORE')) die('Hacking attempt!');

	// render dispatcher list
	function render_dispatcher_list($orders) {
		if (count($orders) > 0) {
			$render = '	<table cellpadding="10" cellspacing="20" width="100%">'.
					  '		<tbody>'.
					  '			<tr>'.
					  '				<th>Имя</th>'.
					  '				<th>Телефон</th>'.
					  '				<th>Адрес</th>'.
					  '				<th>Альбомы</th>'.
					  '				<th>Действия</th>'.
					  '			</tr>';

			foreach ($orders as $order) {
				list($hash, $name, $phone, $address, $albums_id) = $order;
				$render .= "			<tr>".
						   "				<td>$name</td>".
						   "				<td>" . format_phone($phone) . "</td>".
						   "				<td>$address</td>".
						   "				<td>" . render_albums_name_list(get_albums_by_id($albums_id)) . "</td>".
						   "				<td><a href=\"#\" class=\"btn green\" onclick=\"callAction('" . DISPATCHER_APPROVE_ORDER_ACTION . "', { hash:'$hash' }); return false;\">Обработать</a>&nbsp;&nbsp;<a href=\"#\" class=\"btn red\" onclick=\"callAction('" . DISPATCHER_DELETE_ORDER_ACTION . "', { hash:'$hash' }); return false;\">Удалить</a></td>".
						   "			</tr>";
			}
			
			$render .= '		</tbody>'.
					   '	</table>';
		} else {
			$render = '<p>Нет новых заказов.</p>';
		}

		return $render;
	}

?>