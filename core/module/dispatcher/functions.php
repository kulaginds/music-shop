<?php if (!defined('SITECORE')) die('Hacking attempt!');
	
	require_once(CART_DIR.'/functions.php');
	require_once(DISPATCHER_DIR.'/constants.php');

	// check dispatcher login
	function check_dispatcher_login($dispatcher = null) {
		if (is_null($dispatcher)) {
			if (false === array_key_exists(DISPATCHER_AUTH_COOKIE_NAME, $_COOKIE)) {
				return false;
			}

			$dispatcher = $_COOKIE[DISPATCHER_AUTH_COOKIE_NAME];
		}

		$password = md5(SALT.DISPATCHER_PASSWORD);

		return ($password === $dispatcher);
	}

	// login dispatcher
	function login_dispatcher($password) {
		setcookie(DISPATCHER_AUTH_COOKIE_NAME, $password, DISPATCHER_AUTH_COOKIE_MAXTIME, '/');
		$_COOKIE[DISPATCHER_AUTH_COOKIE_NAME] = $password;
	}

	// logout dispatcher
	function logout_dispatcher() {
		setcookie(DISPATCHER_AUTH_COOKIE_NAME, '', DISPATCHER_AUTH_COOKIE_MAXTIME, '/');
		unset($_COOKIE[DISPATCHER_AUTH_COOKIE_NAME]);
	}

	// handler login action
	function dispatcher_handle_login_action() {
		global $error;

		$password = clear_field($_POST['password']);

		if (empty($password)) {
			$error = 'Введен пустой пароль.';

			return;
		}

		$password = md5(SALT.$password);

		if (false === check_dispatcher_login($password)) {
			$error = 'Неверный пароль.';

			return;
		}

		login_dispatcher($password);
	}

	// handler logout action
	function dispatcher_handle_logout_action() {
		logout_dispatcher();
		header('Location: '.DISPATCHER_URL);
		die();
	}

	// handler get new orders action
	function dispatcher_handle_get_new_orders_action() {
		download_file(SECURE_FILE_NEW_ORDERS);
		die();
	}

	// handler get approved orders action
	function dispatcher_handle_get_approved_orders_action() {
		download_file(SECURE_FILE_APPROVED_ORDERS);
		die();
	}

	// handler approve order action
	function dispatcher_approve_order_action() {
		$hash = clear_field($_POST['hash']);

		if (strlen($hash) != 32 && !preg_match('/^[a-f0-9]{32}$/', $hash)) {
			return;
		}

		approve_order($hash);
	}

	// handler delete order action
	function dispatcher_delete_order_action() {
		$hash = clear_field($_POST['hash']);

		if (strlen($hash) != 32 && !preg_match('/^[a-f0-9]{32}$/', $hash)) {
			return;
		}

		delete_orders_by_hash($hash);
	}

	/////////////////////////////////
	// Handling dispatcher actions //
	/////////////////////////////////

	if (check_dispatcher_login()) {
		if (array_key_exists('action', $_POST) && !empty($_POST['action'])) {
			switch($_POST['action']) {
				case DISPATCHER_LOGOUT_ACTION: // handling logout action
					dispatcher_handle_logout_action();
					break;
				case DISPATCHER_GET_NEW_ORDERS_ACTION: // handling get new orders action
					dispatcher_handle_get_new_orders_action();
					break;
				case DISPATCHER_GET_APPROVED_ORDERS_ACTION: // handling get approved orders action
					dispatcher_handle_get_approved_orders_action();
					break;
				case DISPATCHER_APPROVE_ORDER_ACTION: // handling approve order action
					dispatcher_approve_order_action();
					break;
				case DISPATCHER_DELETE_ORDER_ACTION: // handling delete order action
					dispatcher_delete_order_action();
					break;
			}
		}
	} else {
		// handling login action
		if (check_action(DISPATCHER_LOGIN_ACTION)) {
			dispatcher_handle_login_action();
		}
	}

?>