<?php

	// flag of initialization core
	define('SITECORE', 1);

	require_once(__DIR__.'/config/constants.php');
	require_once(CONFIG_DIR.'/site.php');
	require_once(INCLUDE_DIR.'/header.php');

	// show header block
	include(BLOCKS_DIR.'/header.php');

?>