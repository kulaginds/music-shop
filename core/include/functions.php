<?php if (!defined('SITECORE')) die('Hacking attempt!');

	// title formating
	function format_title($page_name = null, $site_name = null) {
		$title = '';

		if (!is_null($page_name)) {
			$title = $page_name;

			if (!is_null($site_name)) {
				$title = $site_name.TITLE_SEPARATOR.$title;
			}
		} elseif (!is_null($site_name)) {
			$title = $site_name;
		}

		return $title;
	}

	// clear field
	function clear_field($field) {
		$field = htmlspecialchars($field);
		$field = strip_tags($field);
		$field = trim($field);

		return $field;
	}

	// check field on limits
	function check_field_length($field, $minlength, $maxlength = null) {
		$len = mb_strlen($field);

		if (is_null($maxlength)) {
			return ($len != $minlength);
		} else {
			return ($len < $minlength || $len > $maxlength);
		}
	}

	// check request action
	function check_action($action) {
		if (false === array_key_exists('action', $_POST)) {
			return false;
		}

		return ($_POST['action'] == $action);
	}

	// do explode and bring item to int
	function explode_to_int($delimiter, $arr) {
		$arr = explode($delimiter, $arr);
		$arr = array_map('_explode_to_int_callback', $arr);

		return $arr;
	}

	// explode_to_int callback function
	function _explode_to_int_callback($item) {
		return @intval($item);
	}

	// download file
	function download_file($file) {
		if (file_exists($file)) {
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
		    header('Content-Disposition: attachment; filename="'.basename($file).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($file));
		    readfile($file);
		    exit;
		} else {
			print 'file not found';
			exit;
		}
	}

?>