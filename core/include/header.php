<?php if (!defined('SITECORE')) die('Hacking attempt!');

	require_once(INCLUDE_DIR.'/helpers.php');
	require_once(INCLUDE_DIR.'/functions.php');

	/////////////
	// modules //
	/////////////

	// albums
	require_once(ALBUMS_DIR.'/helpers.php');
	require_once(ALBUMS_DIR.'/functions.php');
	// cart
	require_once(CART_DIR.'/functions.php');
	// dispatcher
	require_once(DISPATCHER_DIR.'/helpers.php');
	require_once(DISPATCHER_DIR.'/functions.php');

?>