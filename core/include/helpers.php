<?php if (!defined('SITECORE')) die('Hacking attempt!');

	// format phone
	function format_phone($phone) {
		$phone = strval($phone);
		
		return '+'.substr($phone, 0, 1).' ('.substr($phone, 1, 3).') '.substr($phone, 3, 3).' '.substr($phone, 6, 2).' '.substr($phone, 8, 2);
	}

?>