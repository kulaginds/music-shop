<?php if (!defined('SITECORE')) die('Hacking attempt!'); ?>

		<footer>
			<hr>
			<div class="block">
				<?php include(BLOCKS_DIR.'/bottom_block1.php'); ?>
			</div>
			<div class="block">
				<?php include(BLOCKS_DIR.'/bottom_block2.php'); ?>
			</div>
			<div class="block">
				<?php include(BLOCKS_DIR.'/bottom_block3.php'); ?>
			</div>
			<div class="block">
				<?php include(BLOCKS_DIR.'/bottom_block4.php'); ?>
			</div>
			<p><?php echo date("Y"); ?> &copy; <?php echo SITENAME; ?>.</p>
		</footer>

		<script src="<?php echo SITE_URL.'/js/jquery.min.js'; ?>"></script>
		<script src="<?php echo SITE_URL.'/js/jquery.cookie.min.js'; ?>"></script>
		<script src="<?php echo SITE_URL.'/js/jquery.redirect.min.js'; ?>"></script>
		<script src="<?php echo SITE_URL.'/js/common.js'; ?>"></script>

	</body>
</html>