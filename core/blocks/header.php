<?php if (!defined('SITECORE')) die('Hacking attempt!'); ?><html>
	<head>
		<title><?php echo format_title($page_name, SITENAME); ?></title>
		<link rel="stylesheet" href="<?php echo SITE_URL.'/css/common.css'; ?>">
		<meta charset="utf-8">
	</head>
	<body>
		
		<header>
			<div class="logo"><a href="<?php echo HOME_URL; ?>"><?php echo SITENAME; ?></a></div>
			<?php
			
			$default_top_menu = '/top_menu.php';

			if (!isset($top_menu) || empty($top_menu)) {
				$top_menu = $default_top_menu;
			}

			include(BLOCKS_DIR.$top_menu);
			
			?>
			<div class="clear"></div>
			<hr>
		</header>