<section>

	<div class="labels">
		<h3>Корзина для покупок</h3>
		<p>Ниже размещены заказанные вами альбомы.</p>
	</div>
	<hr>

	<?php echo render_albums_list($cart_albums, array(), true); ?>

	<hr>

	<h3 id="total_cost_label">Всего к оплате: $<span id="total_cost"><?php echo get_albums_total_cost($cart_albums); ?></span></h3>

</section>

<section id="order">
	
	<hr>

	<div class="labels">
		<h3>Контактные данные</h3>
		<p>Перед отправкой альбомов мы вам позвоним, чтобы подтвердить заказ.</p>
	</div>
	<hr>

	<form action="<?php echo CART_URL; ?>" method="POST">
		<input type="hidden" name="action" value="<?php echo CART_BUY_ACTION; ?>">
		<table border="0" cellpadding="10">
			<tbody>
				<tr>
					<td>Ваше имя:</td>
					<td><input type="text" name="name" minlength="<?php echo CART_NAME_MINLENGTH; ?>" maxlength="<?php echo CART_NAME_MAXLENGTH; ?>" required></td>
				</tr>
				<tr>
					<td>Ваш телефон:</td>
					<td><input type="text" name="phone" minlength="<?php echo CART_PHONE_LENGTH; ?>" maxlength="<?php echo CART_PHONE_LENGTH; ?>" placeholder="например 71234567891" required></td>
				</tr>
				<tr>
					<td>Ваш почтовый адрес:</td>
					<td><input type="text" name="address" minlength="<?php echo CART_ADDRESS_MINLENGTH; ?>" maxlength="<?php echo CART_ADDRESS_MAXLENGTH; ?>" required></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" class="btn green" value="Купить!"></td>
				</tr>
			</tbody>
		</table>
	</form>

<script>
window.HAS_ALBUMS = <?php echo ($cart_counter > 0) ? 'true' : 'false'; ?>;
</script>