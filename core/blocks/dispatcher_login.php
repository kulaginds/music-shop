<section>
	
	<div class="labels">
		<h3>Вход для диспетчера</h3>
	</div>
	<hr>

	<form action="<?php echo DISPATCHER_URL; ?>" method="POST">
		<input type="hidden" name="action" value="<?php echo DISPATCHER_LOGIN_ACTION; ?>">
	<?php if (isset($error)): ?>
		<p class="error"><?php echo $error; ?></p>
	<?php endif; ?>
		<table border="0" cellpadding="10">
			<tbody>
				<tr>
					<td>Пароль:</td>
					<td><input type="password" name="password" required></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" class="btn" value="Войти"></td>
				</tr>
			</tbody>
		</table>
	</form>

</section>