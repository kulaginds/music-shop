<?php if (!defined('SITECORE')) die('Hacking attempt!');
	
	define('DEBUG', true);

	// pathes
	define('CORE_DIR', realpath(__DIR__.'/..'));
	define('CONFIG_DIR', CORE_DIR.'/config');
	define('INCLUDE_DIR', CORE_DIR.'/include');
	define('BLOCKS_DIR', CORE_DIR.'/blocks');
	define('MODULE_DIR', CORE_DIR.'/module');
	define('SECURE_DIR', CORE_DIR.'/BBC6C3E20C18DD4A6D13262BE9E04591');

	// root url
	define('SITE_URL', ''); // url to site without last slash
	define('HOME_URL', SITE_URL.'/');
	define('CART_URL', SITE_URL.'/cart.php');
	define('DISPATCHER_URL', SITE_URL.'/dispatcher.php');

	// modules
	define('ALBUMS_DIR', MODULE_DIR.'/albums');
	define('CART_DIR', MODULE_DIR.'/cart');
	define('DISPATCHER_DIR', MODULE_DIR.'/dispatcher');

	// secure files
	define('SECURE_FILE_NEW_ORDERS', SECURE_DIR.'/new_orders.csv');
	define('SECURE_FILE_APPROVED_ORDERS', SECURE_DIR.'/approved_orders.csv');

	// misc
	define('SALT', 'E5T$H(.>3sN-.~A9&f-)o{D|8C+k#:YofXpu2gp{|0_BklQIu~FnH$0%MS#*QtMF');

?>